#!/bin/bash
set -e
sudo mkdir -p /usr/local/bin2

cd /usr/local/bin2

sudo rm -f mks
sudo rm -f show
sudo rm -f rms

sudo touch mks

echo 'sudo touch /usr/local/bin2/$1' | sudo tee -a mks
echo 'sudo chmod +x /usr/local/bin2/$1'  | sudo tee -a mks
echo 'sudo vim /usr/local/bin2/$1'  | sudo tee -a mks

sudo chmod +x mks

sudo touch show

echo 'if [ -z $1 ];' | sudo tee -a show
echo '    then ls /usr/local/bin2;' | sudo tee -a show
echo '    else cat /usr/local/bin2/$1;' | sudo tee -a show
echo 'fi' | sudo tee -a show

sudo chmod +x show

sudo touch rms

echo 'sudo rm -f /usr/local/bin2/$1' | sudo tee -a rms

sudo chmod +x rms

cd -

[[ ! -f $(which mks) ]] && echo '\nexport PATH=/usr/local/bin2:$PATH' >> ~/.zshrc
